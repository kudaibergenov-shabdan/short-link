const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const links = require('./app/links');

const port = 8000;
const app = express();
app.use(express.json());
app.use(cors());
app.use('/links', links);

const run = async () => {
    await mongoose.connect('mongodb://localhost/link_db');

    app.listen(port, () => {
       console.log(`Server started on ${port} port !`)
    });
};

run().catch(e => console.log(e));