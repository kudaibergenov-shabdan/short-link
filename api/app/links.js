const express = require('express');
const {nanoid} = require('nanoid');
const Link = require('../models/Link');

const router = express.Router();

const generateUniqueShortUrl = async () => {
    const shortUrl = nanoid(7);
    const shortUrlExisted = await Link.findOne({short_link: shortUrl});
    if (shortUrlExisted) {
        await generateUniqueShortUrl();
    } else {
        return shortUrl;
    }
};

router.post('/', async (req, res) => {
    if (!req.body.original_link) {
        return res.status(400).send({error: 'Data not valid'});
    }
    const shortUrl = await generateUniqueShortUrl();
    const linkData = {
        short_link: shortUrl,
        original_link: req.body.original_link
    };
    const link = new Link(linkData);
    try {
        await link.save();
        res.send(shortUrl);
    } catch {
        res.sendStatus(500);
    }
});

router.get('/:shortUrl', async (req, res) => {
    try {
        const link = await Link.findOne({short_link: req.params.shortUrl});

        if (link) {
            res.status(301).redirect(link.original_link);
        } else {
            res.status(404).send({error: 'Original link not found'})
        }
    }
    catch {
        res.sendStatus(500);
    }
});

module.exports = router;