const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const LinkSchema = new Schema({
    short_link: {
        type: String,
        required: true
    },
    original_link: {
        type: String,
        required: true
    }
});

const Link = mongoose.model('Link', LinkSchema);
module.exports = Link;