import React from 'react';
import './Home.css';
import {useDispatch, useSelector} from "react-redux";
import {changeOriginalLink, shortenLink} from "../store/action";

const Home = () => {
    const linkApi = 'http://localhost:8000/links/';
    const dispatch = useDispatch();
    const originalLink = useSelector(state => state.originalLink);
    const shortLink = useSelector(state => state.shortLink);

    const onInputChange = e => {
        const originalLink = e.target.value;
        dispatch(changeOriginalLink(originalLink));
    };

    const onSubmitShortenForm = e => {
        e.preventDefault();
        dispatch(shortenLink(originalLink));
    };

    return (
        <div className="Home">
            <form
                className="Shorten"
                onSubmit={onSubmitShortenForm}
            >
                <h1>Shorten your link</h1>
                <div>
                    <input
                        type="text"
                        className="Shorten__original-link"
                        name="originalLink"
                        value={originalLink}
                        onChange={onInputChange}
                    />
                </div>
                <div>
                    <button className="Shorten__btn-shorten" type="submit">Shorten!</button>
                </div>
                <div>
                    <a className="Shorten__short-link"
                       href={linkApi + shortLink}
                    >
                        {shortLink && (linkApi + shortLink)}
                    </a>
                </div>
            </form>
        </div>
    );
};

export default Home;