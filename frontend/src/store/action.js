import axios from "axios";

export const CHANGE_ORIGINAL_LINK = 'CHANGE_ORIGINAL_LINK';
export const CHANGE_SHORT_LINK = 'CHANGE_SHORT_LINK';

export const changeOriginalLink = originalLink => ({type: CHANGE_ORIGINAL_LINK, payload: originalLink});
export const changeShortLink = shortLink => ({type: CHANGE_SHORT_LINK, payload: shortLink});

export const shortenLink = originalLink => {
    return async dispatch => {
        try {
            const response = await axios.post('http://localhost:8000/links/', {
                original_link: originalLink
            });
            dispatch(changeShortLink(response.data));
        } catch {
            console.log('Smth went wrong in method shortenLink');
        }
    }
}
