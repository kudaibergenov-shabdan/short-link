import {CHANGE_ORIGINAL_LINK, CHANGE_SHORT_LINK} from "./action";

const initialState = {
    originalLink: "",
    shortLink: "",
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_ORIGINAL_LINK:
            return {...state, originalLink: action.payload, shortLink: ""};
        case CHANGE_SHORT_LINK:
            return {...state, shortLink: action.payload};
        default:
            return state;
    }
};

export default reducer;